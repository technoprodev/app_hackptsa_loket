if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            permohonan: {
                jenis_permohonan: null,
                id_jenis_permintaan_dok: null,
                permohonanDokumens: [],
            },
            jenisPermintaanDoks: [],
            ijpd: null,
        },
        methods: {
            addPermohonanDokumen: function() {
                this.dev.permohonanDokumens.push({
                    id: null,
                    id_permohonan: null,
                    nama_file: null
                });
            },
            removePermohonanDokumen: function(i, isNew) {
                if (this.dev.permohonanDokumens[i].id == null)
                    this.dev.permohonanDokumens.splice(i, 1);
                else
                    this.dev.permohonanDokumens[i].id*=-1;
            },
        },
        computed: {
            index_jenis_permintaan_dok: function () {
                for (var i = 0; i < this.jenisPermintaanDoks.length; i++) {
                    if (this.permohonan.id_jenis_permintaan_dok == this.jenisPermintaanDoks[i].id) {
                        this.ijpd = i;
                        return i;
                    }
                }
            }
        },
    });
}