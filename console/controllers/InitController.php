<?php
namespace app_hackptsa_loket\console\controllers;

use yii\console\Controller;
use technosmart\models\Permission;

class InitController extends Controller
{
    public $dir = [
        'app_hackptsa_loket\controllers' => '@app_hackptsa_loket/controllers',
    ];

    public function actionIndex()
    {
        \Yii::$app->runAction('cache/flush-all');

        \Yii::$app->cache->cachePath = \Yii::getAlias('@app_hackptsa_loket/runtime/cache');
        \Yii::$app->runAction('cache/flush-all');

        if (Permission::initAllController($this->dir))
            return 0;
        else
            return 1;
    }
}