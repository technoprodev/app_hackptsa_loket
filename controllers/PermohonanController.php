<?php
namespace app_hackptsa_loket\controllers;

use Yii;
use app_hackptsa_admin\models\Pemohon;
use app_hackptsa_admin\models\Permohonan;
use app_hackptsa_admin\models\PermohonanDokumen;
use app_hackptsa_admin\models\Mediator;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

class PermohonanController extends \technosmart\yii\web\Controller
{
    protected function findModelPermohonanKonsultasi($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'konsultasi', 'id_petugas_loket' => null])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPermohonanSoftcopy($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'dokumen', 'status_dokumen' => 'diajukan'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPermohonanHardcopy($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'dokumen', 'status_dokumen' => 'menunggu verifikasi'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPermohonanSedangDiproses($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'dokumen', 'status_dokumen' => 'sedang diproses'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPermohonanDokumen($id)
    {
        if (($model = PermohonanDokumen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionCreate()
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = new Permohonan();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);
            if (isset($post['PermohonanDokumen'])) {
                foreach ($post['PermohonanDokumen'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $permohonanDokumen = $this->findModelPermohonanDokumen($value['id']);
                        $permohonanDokumen->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $permohonanDokumen = $this->findModelPermohonanDokumen(($value['id']*-1));
                        $permohonanDokumen->isDeleted = true;
                    } else {
                        $permohonanDokumen = new PermohonanDokumen();
                        $permohonanDokumen->setAttributes($value);
                    }
                    // Yii::$app->d->dd(UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload"));

                    // $temp = UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload");
                    // $name = $permohonanDokumen->virtual_nama_file_upload->name;
                    // $permohonanDokumen->nama_file = $name;
                    // $permohonanDokumen->nama_file = $temp->name;
                    // $permohonanDokumen->virtual_nama_file_upload = $temp;
                    /*Yii::$app->d->dd($permohonanDokumen->nama_file);
                    Yii::$app->d->dd($permohonanDokumen->virtual_nama_file_upload);
                    Yii::$app->d->ddx($permohonanDokumen);*/

                    /*$uploadRoot = Yii::$app->params['configurations_file']['permohonan_dokumen-nama_file']['alias_upload_root'];
                    $path = Yii::getAlias($uploadRoot) . '/' . $permohonanDokumen->id;
                    if ( !is_dir($path) ) mkdir($path);
                    UploadedFile::getInstance($permohonanDokumen, "[$key]virtual_nama_file_upload")->saveAs($path . '/' . 'asdf');*/
                    
                    $permohonanDokumen->nama_file="dami dulu";
                    $model['permohonan_dokumen'][] = $permohonanDokumen;
                }
            }
            // Yii::$app->d->ddx($model['permohonan_dokumen']);
            // Yii::$app->d->ddx($_FILES);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            // try {
                if ($pemohon = Pemohon::findOne(['nik' => $model['permohonan']->nik])) {
                    $model['permohonan']->id_pemohon = $pemohon->id;
                }
                if ($model['permohonan']->jenis_permohonan == 'konsultasi' || $model['permohonan']->jenis_permohonan == 'konsultasi mediasi') {
                    $model['permohonan']->id_petugas_loket = Yii::$app->user->identity->id;
                }
                if ($model['permohonan']->jenis_permohonan == 'konsultasi mediasi') {
                    $model['permohonan']->scenario = 'konsultasi mediasi';
                }
                if ($model['permohonan']->jenis_permohonan == 'dokumen') {
                    $model['permohonan']->status_dokumen = 'sedang diproses';
                }
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $error = false;
                if (isset($model['permohonan_dokumen']) and is_array($model['permohonan_dokumen'])) {
                    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
                        if ($permohonanDokumen->isDeleted) {
                            if (!$permohonanDokumen->delete()) {
                                $error = true;
                            }
                        } else {
                            $permohonanDokumen->id_permohonan = $model['permohonan']->id;
                            if (!$permohonanDokumen->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['permohonan']->commit();
                if ($model['permohonan']->jenis_permohonan == 'dokumen') {
                    Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Permohonan Anda sedang diproses dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " dan akan selesai pada tanggal " . $model['permohonan']->estimasi_hari);
                } else if ($model['permohonan']->jenis_permohonan == 'konsultasi mediasi') {
                    if ($model['permohonan']->nomor_hp_mediator) {
                        Permohonan::smsNotifikasi($model['permohonan']->nomor_hp_mediator, "Anda dibutuhkan di loket PTSA, harap segera datang.");
                    } else {
                        $mediator = Mediator::findOne($model['permohonan']->id_mediator);
                        if ($mediator) {
                            Permohonan::smsNotifikasi($mediator->nomor_hp, "Anda dibutuhkan di loket PTSA, harap segera datang.");
                        }
                    }
                }
                Yii::$app->session->setFlash('success', 'Permohonan berhasil disimpan.');
            /*} catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }*/
        } else {
            foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;

            if ($model['permohonan']->isNewRecord) {
                $model['permohonan']->nomor_permohonan = $this->sequence('permohonan-nomor_permohonan');
                $model['permohonan']->scenario = 'form-create';
                // $model['permohonan_dokumen'][] = new PermohonanDokumen();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form-create', [
                'model' => $model,
                'title' => 'Tambah Permohonan',
            ]);
        else
            return $this->redirect(['create']);
    }

    public function actionDatatablesListKonsultasi()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->where('p.jenis_permohonan = "konsultasi"')
                ->andWhere('p.id_petugas_loket IS NULL')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListKonsultasi()
    {
        return $this->render('list-konsultasi', [
            'title' => 'Permohonan Konsultasi',
        ]);
    }

    public function actionKonsultasi($nomor_permohonan)
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = $this->findModelPermohonanKonsultasi($nomor_permohonan);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                if ($model['permohonan']->jenis_permohonan == 'konsultasi') {
                    $model['permohonan']->id_petugas_loket = Yii::$app->user->identity->id;
                }
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['permohonan']->commit();
                Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Permohonan Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " telah di respon, silahkan kunjungi Aplikasi PTSA menggunakan no Hp yang terdaftar.");
                Yii::$app->session->setFlash('success', 'Komentar Anda telah dikirim ke pemohon.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            /*foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;*/

            $render = true;
        }

        if ($render)
            return $this->render('form-konsultasi', [
                'model' => $model,
                'title' => 'Tanggapi Permohonan',
            ]);
        else
            return $this->redirect(['list-konsultasi']);
    }

    public function actionDatatablesListSoftcopy()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->join('JOIN', 'jenis_permintaan_dok jpd', 'jpd.id = p.id_jenis_permintaan_dok')
                ->where('p.jenis_permohonan = "dokumen"')
                ->andWhere('p.status_dokumen = "diajukan"')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'jpd.jenis_permintaan_dok AS jenis_permintaan_dokumen',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListSoftcopy()
    {
        return $this->render('list-softcopy', [
            'title' => 'Verifikasi Berkas Softcopy',
        ]);
    }

    public function actionSoftcopy($nomor_permohonan)
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = $this->findModelPermohonanSoftcopy($nomor_permohonan);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                if ($model['permohonan']->jenis_permohonan == 'dokumen') {
                    if ($model['permohonan']->status_dokumen == 'Tolak') {
                        $model['permohonan']->status_dokumen = 'ditolak';
                        $model['permohonan']->rejected_by = Yii::$app->user->identity->id;
                    } else {
                        $model['permohonan']->verified_softcopy_by = Yii::$app->user->identity->id;
                        $model['permohonan']->status_dokumen = 'menunggu verifikasi';
                    }
                }
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                if ($model['permohonan']->status_dokumen == 'menunggu verifikasi') {
                    Yii::$app->session->setFlash('success', 'Berkas Softcopy berhasil diverifikasi. Dokumen Hardcopy siap diverifikasi.');
                    Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Berkas Softcopy Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " telah di verifikasi, silahkan datang ke Kantor untuk verifikasi berkas hardcopy.");
                } else {
                    Yii::$app->session->setFlash('success', 'Berkas Softcopy berhasil ditolak. Dokumen tidak akan diproses.');
                    Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Berkas Softcopy Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " ditolak, kunjungi Aplikasi PTSA untuk melihat catatan oleh petugas.");
                }

                $transaction['permohonan']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            /*foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;*/

            $render = true;
        }

        if ($render)
            return $this->render('form-softcopy', [
                'model' => $model,
                'title' => 'Verifikasi Berkas Softcopy '.$model['permohonan']->nomor_permohonan,
            ]);
        else
            return $this->redirect(['list-softcopy']);
    }

    public function actionDatatablesListHardcopy()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->join('JOIN', 'jenis_permintaan_dok jpd', 'jpd.id = p.id_jenis_permintaan_dok')
                ->where('p.jenis_permohonan = "dokumen"')
                ->andWhere('p.status_dokumen = "menunggu verifikasi"')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'jpd.jenis_permintaan_dok AS jenis_permintaan_dokumen',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListHardcopy()
    {
        return $this->render('list-hardcopy', [
            'title' => 'Verifikasi Berkas Hardcopy',
        ]);
    }

    public function actionHardcopy($nomor_permohonan)
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = $this->findModelPermohonanHardcopy($nomor_permohonan);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                if ($model['permohonan']->jenis_permohonan == 'dokumen') {
                    if ($model['permohonan']->status_dokumen == 'Tolak') {
                        $model['permohonan']->status_dokumen = 'ditolak';
                        $model['permohonan']->rejected_by = Yii::$app->user->identity->id;
                        $model['permohonan']->estimasi_hari = null;
                    } else {
                        $model['permohonan']->verified_hardcopy_by = Yii::$app->user->identity->id;
                        $model['permohonan']->status_dokumen = 'sedang diproses';
                    }
                }
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                if ($model['permohonan']->status_dokumen == 'sedang diproses') {
                    Yii::$app->session->setFlash('success', 'Berkas Hardcopy berhasil diverifikasi. Dokumen akan segera diproses.');
                    Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Berkas Hardcopy Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " telah di verifikasi, dan akan selesai pada tanggal " . $model['permohonan']->estimasi_hari);
                } else {
                    Yii::$app->session->setFlash('success', 'Berkas Hardcopy berhasil ditolak. Dokumen tidak akan diproses.');
                    Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Berkas Hardcopy Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " telah di tolak, kunjungi Aplikasi PTSA untuk melihat catatan oleh petugas.");
                }

                $transaction['permohonan']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            /*foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;*/

            $render = true;
        }

        if ($render)
            return $this->render('form-hardcopy', [
                'model' => $model,
                'title' => 'Verifikasi Berkas Hardcopy ' . $model['permohonan']->nomor_permohonan,
            ]);
        else
            return $this->redirect(['list-hardcopy']);
    }

    public function actionDatatablesListSedangDiproses()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->join('JOIN', 'jenis_permintaan_dok jpd', 'jpd.id = p.id_jenis_permintaan_dok')
                ->where('p.jenis_permohonan = "dokumen"')
                ->andWhere('p.status_dokumen = "sedang diproses"')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'jpd.jenis_permintaan_dok AS jenis_permintaan_dokumen',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
                'p.komentar',
                'p.estimasi_hari'
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListSedangDiproses()
    {
        return $this->render('list-sedang-diproses', [
            'title' => 'Dokumen yang sedang diproses',
        ]);
    }

    public function actionSelesaikan($nomor_permohonan)
    {
        // Yii::$app->d->ddx(Yii::$app->user->identity->id);
        $render = false;

        $model['permohonan'] = $this->findModelPermohonanSedangDiproses($nomor_permohonan);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['permohonan']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['permohonan'])
                );
                return $this->json($result);
            }

            $transaction['permohonan'] = Permohonan::getDb()->beginTransaction();

            try {
                if ($model['permohonan']->jenis_permohonan == 'dokumen') {   
                    $model['permohonan']->accept_by = Yii::$app->user->identity->id;
                    $model['permohonan']->status_dokumen = 'selesai';
                }
                if (!$model['permohonan']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                Yii::$app->session->setFlash('success', 'Dokumen telah selesai diproses');
                Permohonan::smsNotifikasi($model['permohonan']->nomor_hp, "Permohonan Anda dengan No Permohonan " . $model['permohonan']->nomor_permohonan . " telah selesai, harap datang ke Kantor untuk mengambil dokumen.");

                $transaction['permohonan']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['permohonan']->rollBack();
            }
        } else {
            /*foreach ($model['permohonan']->permohonanDokumens as $key => $permohonanDokumen)
                $model['permohonan_dokumen'][] = $permohonanDokumen;*/

            $render = true;
        }

        if ($render)
            return $this->render('form-selesaikan', [
                'model' => $model,
                'title' => 'Selesaikan Permohonan ' . $model['permohonan']->nomor_permohonan,
            ]);
        else
            return $this->redirect(['list-sedang-diproses']);
    }
}