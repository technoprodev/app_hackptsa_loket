<?php
namespace app_hackptsa_loket\controllers;

use Yii;
use app_hackptsa_admin\models\Permohonan;
use app_hackptsa_admin\models\PermohonanDokumen;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class RiwayatController extends \technosmart\yii\web\Controller
{
    protected function findModelRiwayatDokumen($nomor_permohonan)
    {
        if (($model = Permohonan::findOne(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'dokumen', 'status_dokumen' => 'selesai'])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelRiwayatKonsultasi($nomor_permohonan)
    {
        if (($model = Permohonan::find()->where(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'konsultasi'])->andWhere('id_petugas_loket IS NOT NULL')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelRiwayatKonsultasiMediasi($nomor_permohonan)
    {
        if (($model = Permohonan::find()->where(['nomor_permohonan' => $nomor_permohonan, 'jenis_permohonan' => 'konsultasi mediasi'])->andWhere('id_petugas_loket IS NOT NULL')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //////////////

    public function actionDatatablesListDokumen()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->join('JOIN', 'jenis_permintaan_dok jpd', 'jpd.id = p.id_jenis_permintaan_dok')
                ->where('p.jenis_permohonan = "dokumen"')
                ->andWhere('p.status_dokumen = "selesai"')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'jpd.jenis_permintaan_dok AS jenis_permintaan_dokumen',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
                'p.komentar',
                'p.estimasi_hari'
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListDokumen()
    {
        return $this->render('list-dokumen', [
            'title' => 'Riwayat Dokumen'
        ]);
    }

    public function actionDokumen($nomor_permohonan)
    {
        $model['permohonan'] = $this->findModelRiwayatDokumen($nomor_permohonan);

        return $this->render('one-dokumen', [
            'model' => $model,
            'title' => 'Riwayat Dokumen ' . $model['permohonan']->nomor_permohonan,
        ]);
    }

    public function actionDatatablesListKonsultasi()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->where('p.jenis_permohonan = "konsultasi"')
                ->andWhere('p.id_petugas_loket IS NOT NULL')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListKonsultasi()
    {
        return $this->render('list-konsultasi', [
            'title' => 'Riwayat Konsultasi'
        ]);
    }

    public function actionKonsultasi($nomor_permohonan)
    {
        $model['permohonan'] = $this->findModelRiwayatKonsultasi($nomor_permohonan);

        return $this->render('one-konsultasi', [
            'model' => $model,
            'title' => 'Riwayat Konsultasi ' . $model['permohonan']->nomor_permohonan,
        ]);
    }

    public function actionDatatablesListKonsultasiMediasi()
    {
        $db = Permohonan::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('permohonan p')
                ->join('JOIN', 'mediator m', 'm.id = p.id_mediator')
                ->where('p.jenis_permohonan = "konsultasi mediasi"')
                ->andWhere('p.id_petugas_loket IS NOT NULL')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'p.nomor_permohonan',
                'p.nomor_permohonan',
                'p.created_at',
                'p.nik',
                'p.nama',
                'p.keperluan',
                'm.nama AS mediator'
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionListKonsultasiMediasi()
    {
        return $this->render('list-konsultasi-mediasi', [
            'title' => 'Riwayat Konsultasi Mediasi'
        ]);
    }

    public function actionKonsultasiMediasi($nomor_permohonan)
    {
        $model['permohonan'] = $this->findModelRiwayatKonsultasiMediasi($nomor_permohonan);
        // Yii::$app->d->ddx($model['permohonan']);

        return $this->render('one-konsultasi-mediasi', [
            'model' => $model,
            'title' => 'Riwayat Konsultasi Mediasi ' . $model['permohonan']->nomor_permohonan,
        ]);
    }
}