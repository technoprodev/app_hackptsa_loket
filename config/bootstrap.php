<?php
Yii::setAlias('@app_hackptsa_loket', dirname(dirname(__DIR__)) . '/app_hackptsa_loket');
Yii::setAlias('@app_hackptsa_admin', dirname(dirname(__DIR__)) . '/app_hackptsa_admin');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@download-dev-file', 'http://localhost/technosmart/app_hackptsa_loket/web/upload/dev-file');
Yii::setAlias('@upload-dev-file', dirname(dirname(__DIR__)) . '/web/upload/dev-file');