<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['signup']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['signup'], ['class' => '']);
}
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="50px;" class="bg-dark-azure shadow circle padding-5">
</div>
<div class="margin-top-20 border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-azure text-center fs-18">
        <span class="f-italic"><?= $this->title ?></span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'app']); ?>

        <?= $form->field($model['signup'], 'email')->begin(); ?>
            <?= Html::activeLabel($model['signup'], 'email', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['signup'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['signup'], 'email', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['signup'], 'email')->end(); ?>

        <?= $form->field($model['signup'], 'password')->begin(); ?>
            <?= Html::activeLabel($model['signup'], 'password', ['class' => 'control-label']); ?>
            <?= Html::activePasswordInput($model['signup'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['signup'], 'password', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['signup'], 'password')->end(); ?>

        <?= $form->field($model['signup'], 'password_repeat')->begin(); ?>
            <?= Html::activeLabel($model['signup'], 'password_repeat', ['class' => 'control-label']); ?>
            <?= Html::activePasswordInput($model['signup'], 'password_repeat', ['class' => 'form-control', 'maxlength' => true]); ?>
            <?= Html::error($model['signup'], 'password_repeat', ['class' => 'help-block help-block-error']); ?>
        <?= $form->field($model['signup'], 'password_repeat')->end(); ?>

        <?php if ($error) : ?>
            <div class="alert alert-danger">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Signup', ['class' => 'btn border-azure bg-azure hover-bg-light-azure']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="bg-lighter padding-y-10">
        <div class="text-center text-azure fs-18"><?= Yii::$app->params['app.name'] ?></div>
        <div class="text-center fs-12 margin-top-2"><?= Yii::$app->params['app.author'] ?> © <?= date('Y') ?></div>
    </div>
</div>