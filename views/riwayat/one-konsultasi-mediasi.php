<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

//
/*$permohonanDokumens = [];
if (isset($model['permohonan_dokumen']))
    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen)
        $permohonanDokumens[] = $permohonanDokumen->attributes;*/

/*$this->registerJs(
    'vm.$data.permohonan.permohonanDokumens = vm.$data.permohonan.permohonanDokumens.concat(' . json_encode($permohonanDokumens) . ');',
    // 'vm.$data.permohonan.permohonanDokumens = Object.assign({}, vm.$data.permohonan.permohonanDokumens, ' . json_encode($permohonanDokumens) . ');',
    3
);*/
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<div>
    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_permohonan'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_permohonan ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nik'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nik ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_hp'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_hp ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nama'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nama ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['email'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->email ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['alamat'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->alamat ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">Mediator</label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->mediator->nama ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['keperluan'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->keperluan ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['komentar'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->komentar ?></p>
    </div>

</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>