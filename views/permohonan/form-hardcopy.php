<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
/*$permohonanDokumens = [];
if (isset($model['permohonan_dokumen']))
    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen)
        $permohonanDokumens[] = $permohonanDokumen->attributes;*/

/*$this->registerJs(
    'vm.$data.permohonan.permohonanDokumens = vm.$data.permohonan.permohonanDokumens.concat(' . json_encode($permohonanDokumens) . ');',
    // 'vm.$data.permohonan.permohonanDokumens = Object.assign({}, vm.$data.permohonan.permohonanDokumens, ' . json_encode($permohonanDokumens) . ');',
    3
);*/

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['permohonan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['permohonan'], ['class' => '']);
}

/*if (isset($model['permohonan_dokumen'])) foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
    if ($permohonanDokumen->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($permohonanDokumen, ['class' => '']);
        $errorVue = true; 
    }
}*/
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_permohonan'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_permohonan ?></p>
    </div>

    <div class="form-group">
        <label class="control-label">Jenis Permintaan Dokumen</label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->jenisPermintaanDok->jenis_permintaan_dok ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nik'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nik ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_hp'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_hp ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nama'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nama ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['email'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->email ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['alamat'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->alamat ?></p>
    </div>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['keperluan'] ?></label>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->keperluan ?></p>
    </div>

    <?= $form->field($model['permohonan'], 'komentar')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'komentar', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'komentar', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'komentar', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'komentar')->end(); ?>

    <?= $form->field($model['permohonan'], 'estimasi_hari')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'estimasi_hari', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'estimasi_hari', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'estimasi_hari', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'estimasi_hari')->end(); ?>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Siap diproses', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::submitInput('Tolak', ['class' => 'btn btn-default rounded-xs border-azure text-azure', 'name' => 'Permohonan[status_dokumen]']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>