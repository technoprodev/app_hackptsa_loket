<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$permohonanDokumens = [];
if (isset($model['permohonan_dokumen']))
    foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
        $pd = $permohonanDokumen->attributes;
        $pd['virtual_nama_file_upload'] = $permohonanDokumen->virtual_nama_file_upload;
        $pd['virtual_nama_file_download'] = $permohonanDokumen->virtual_nama_file_download;
        $permohonanDokumens[] = $permohonanDokumen->attributes;
    }

$jenisPermintaanDoks = [];
$jpds = \app_hackptsa_admin\models\JenisPermintaanDok::find()->with('jenisBerkases')->all();
foreach ($jpds as $key => $jenisPermintaanDok) {
    $jenisBerkases = [];
    $jbs = $jenisPermintaanDok->jenisBerkases;
    foreach ($jbs as $key => $jenisBerkas) {
        $jb = $jenisBerkas->attributes;
        $jenisBerkases[] = $jb;
    }

    $temp = $jenisPermintaanDok->attributes;
    $temp['jenisBerkases'] = $jenisBerkases;

    $jenisPermintaanDoks[] = $temp;
}
// Yii::$app->d->ddx($jenisPermintaanDoks);

$this->registerJs(
    'vm.$data.jenisPermintaanDoks = vm.$data.jenisPermintaanDoks.concat(' . json_encode($jenisPermintaanDoks) . ');'.
    'vm.$data.permohonan.id_jenis_permintaan_dok = ' . json_encode($model['permohonan']->id_jenis_permintaan_dok) . ';'.
    'vm.$data.permohonan.permohonanDokumens = vm.$data.permohonan.permohonanDokumens.concat(' . json_encode($permohonanDokumens) . ');',
    // 'vm.$data.permohonan.permohonanDokumens = Object.assign({}, vm.$data.permohonan.permohonanDokumens, ' . json_encode($permohonanDokumens) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['permohonan']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['permohonan'], ['class' => '']);
}

if (isset($model['permohonan_dokumen'])) foreach ($model['permohonan_dokumen'] as $key => $permohonanDokumen) {
    if ($permohonanDokumen->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($permohonanDokumen, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['permohonan']->attributeLabels()['nomor_permohonan'] ?></label>
        <?= Html::activeHiddenInput($model['permohonan'], 'nomor_permohonan'); ?>
        <p class="form-control-static border-bottom"><?= $model['permohonan']->nomor_permohonan ?></p>
    </div>

    <?= $form->field($model['permohonan'], 'nik')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'nik', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'nik', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'nik', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'nik')->end(); ?>

    <?= $form->field($model['permohonan'], 'nama')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'nama', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'nama', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'nama', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'nama')->end(); ?>

    <?= $form->field($model['permohonan'], 'nomor_hp')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'nomor_hp', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'nomor_hp', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'nomor_hp', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'nomor_hp')->end(); ?>

    <?= $form->field($model['permohonan'], 'email')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'email', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'email', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'email')->end(); ?>

    <?= $form->field($model['permohonan'], 'alamat')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'alamat', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'alamat', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'alamat', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'alamat')->end(); ?>

    <?= $form->field($model['permohonan'], 'keperluan')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'keperluan', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'keperluan', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'keperluan', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'keperluan')->end(); ?>

    <?= $form->field($model['permohonan'], 'komentar')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'komentar', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['permohonan'], 'komentar', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'komentar', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'komentar')->end(); ?>

    <?= $form->field($model['permohonan'], 'jenis_permohonan', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'jenis_permohonan', ['class' => 'control-label']); ?>
        <?= Html::activeRadioList($model['permohonan'], 'jenis_permohonan', $model['permohonan']->getEnum('jenis_permohonan'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $checked = $checked ? 'checked' : '';
                $disabled = in_array($value, []) ? 'disabled' : '';
                return "<div class='radio col-xs-4'><label><input type='radio' name='$name' value='$value' v-model='permohonan.jenis_permohonan' $checked $disabled><i></i>$label</label></div>";
            }]); ?>
        <?= Html::error($model['permohonan'], 'jenis_permohonan', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'jenis_permohonan')->end(); ?>

    <div v-if="(permohonan.jenis_permohonan=='dokumen')" v-init>

    <?= $form->field($model['permohonan'], 'id_jenis_permintaan_dok')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'id_jenis_permintaan_dok', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['permohonan'], 'id_jenis_permintaan_dok', ArrayHelper::map(\app_hackptsa_admin\models\JenisPermintaanDok::find()->indexBy('id')->asArray()->all(), 'id', 'jenis_permintaan_dok'), ['prompt' => 'Choose one please', 'class' => 'form-control', 'v-model' => 'permohonan.id_jenis_permintaan_dok']); ?>
        <?= Html::error($model['permohonan'], 'id_jenis_permintaan_dok', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'id_jenis_permintaan_dok')->end(); ?>

    <?= $form->field($model['permohonan'], 'estimasi_hari')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'estimasi_hari', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'estimasi_hari', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'estimasi_hari', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'estimasi_hari')->end(); ?>


    <template v-for="(value, key, index) in jenisPermintaanDoks">
        <template v-for="(v, k, i) in value.jenisBerkases">
            <template v-if="value.id == permohonan.id_jenis_permintaan_dok">
                <input type="hidden" v-bind:id="'permohonandokumen-' + k + '-id'" v-bind:name="'PermohonanDokumen[' + k + '][id]'" class="form-control" type="text">
                <input type="hidden" v-bind:id="'permohonandokumen-' + k + '-id_jenis_berkas'" v-bind:name="'PermohonanDokumen[' + k + '][id_jenis_berkas]'" class="form-control" type="text" v-bind:value="v.id">
                <div v-bind:class="'form-group field-permohonandokumen-' + k + '-virtual_nama_file_upload'">
                    <label v-bind:for="'permohonandokumen-' + k + '-virtual_nama_file_upload'" class="control-label">{{v.nama_berkas}}</label>
                    <!-- <input v-bind:id="'permohonandokumen-' + k + '-virtual_nama_file_upload'" v-bind:name="'PermohonanDokumen[' + k + '][virtual_nama_file_upload]'" class="form-control" type="text"> -->
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"><a v-bind:href="v.virtual_nama_file_download">{{v.file}}</a></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Select file</span>
                            <span class="fileinput-exists">Change</span>
                            <input v-bind:id="'permohonandokumen-' + k + '-virtual_nama_file_upload'" v-bind:name="'PermohonanDokumen[' + k + '][virtual_nama_file_upload]'" aria-invalid="false" type="file">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                    <div class="help-block"></div>
                </div>
            </template>
        </template>
    </template>

    <!-- <template v-if="typeof jenis_permintaan_doks[permohonan.id_jenis_permintaan_dok].jenisBerkases == 'object'">
        <template v-for="(value, key, index) in jenis_permintaan_doks[permohonan.id_jenis_permintaan_dok].jenisBerkases">
            {{value.id}}
        </template>
    </template>

    jenis_permintaan_doks.map(function(x) {return x.id; }).indexOf(permohonan.id_jenis_permintaan_dok) -->

    </div>
    
    <div v-if="(permohonan.jenis_permohonan=='konsultasi mediasi')" v-init>

    <?= $form->field($model['permohonan'], 'id_mediator')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'id_mediator', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['permohonan'], 'id_mediator',
            ArrayHelper::map(
                \Yii::$app->dba->createCommand(
                    "SELECT m.id, CONCAT(m.nama, ' - ', COALESCE(d.departemen, ''), ' (no Hp: ' , COALESCE(m.nomor_hp, 'tidak ada') , ')') as nama
                    FROM `mediator` m
                    JOIN `departemen` d ON d.id = m.id_departemen
                    "
                )->queryAll(),
                'id',
                'nama'
            ),
            ['prompt' => 'Choose one please', 'class' => 'form-control select2']); ?>
        <?= Html::error($model['permohonan'], 'id_mediator', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'id_mediator')->end(); ?>

    <?= $form->field($model['permohonan'], 'nomor_hp_mediator')->begin(); ?>
        <?= Html::activeLabel($model['permohonan'], 'nomor_hp_mediator', ['class' => 'control-label', 'label' => 'Nomor Hp Mediator (Diisi jika nomor yang terdaftar tidak aktif)']); ?>
        <?= Html::activeTextInput($model['permohonan'], 'nomor_hp_mediator', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['permohonan'], 'nomor_hp_mediator', ['class' => 'help-block']); ?>
    <?= $form->field($model['permohonan'], 'nomor_hp_mediator')->end(); ?>

    </div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>