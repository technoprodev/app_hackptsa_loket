<?php
namespace app_hackptsa_loket\models;

use Yii;
use yii\base\Model;

class Login extends Model
{
    public $login;
    public $username;
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;

    public function rules()
    {
        return [
            ['login', 'required', 'on' => 'using-login'],
            ['username', 'required', 'on' => 'using-username'],
            ['email', 'required', 'on' => 'using-email'],
            
            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Email or Username',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($user = $this->getUser()) {
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        } else {
            switch ($this->scenario) {
                case 'using-login':
                    $this->addError('login', 'Username or email is not exists.');
                    break;
                case 'using-username':
                    $this->addError('username', 'Username is not exists.');
                    break;
                case 'using-email':
                    $this->addError('email', 'Email is not exists.');
                    break;
                default:
                    $this->addError('login', 'Username or email is not exists.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    protected function getUser()
    {
        if ($this->_user === null) {
            switch ($this->scenario) {
                case 'using-login':
                    $this->_user = User::findByLogin($this->login);
                    break;
                case 'using-username':
                    $this->_user = User::findByUsername($this->username);
                    break;
                case 'using-email':
                    $this->_user = User::findByEmail($this->email);
                    break;
                default:
                    $this->_user = User::findByLogin($this->login);
            }
        }

        return $this->_user;
    }
}
