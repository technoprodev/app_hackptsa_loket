<?php
namespace app_hackptsa_loket\models;

use Yii;

/**
 * This is the model class for table "dev_category_option".
 *
 * @property integer $id
 * @property string $name
 * @property string $parent
 *
 * @property Dev[] $devs
 * @property DevCategory[] $devCategories
 */
class DevCategoryOption extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'dev_category_option';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],

            //parent
            [['parent'], 'required'],
            [['parent'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevs()
    {
        return $this->hasMany(Dev::className(), ['link' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevCategories()
    {
        return $this->hasMany(DevCategory::className(), ['id_dev_category_option' => 'id']);
    }
}
