<?php
namespace app_hackptsa_loket\models;

use yii\base\Model;
use technosmart\models\User;

class Signup extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['username'], 'trim', 'when' => function($model) {
                return $model->username != NULL;
            }],
            ['username', 'required', 'on' => 'using-username'],
            ['username', 'required', 'on' => 'using-username-email'],
            ['username', 'unique', 'targetClass' => '\technosmart\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],

            [['email'], 'trim', 'when' => function($model) {
                return $model->email != NULL;
            }],
            ['email', 'required', 'on' => 'using-email'],
            ['email', 'required', 'on' => 'using-username-email'],
            ['email', 'unique', 'targetClass' => '\technosmart\models\User', 'message' => 'This email address has already been taken.'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>'Passwords do not match'],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->status = 1;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
